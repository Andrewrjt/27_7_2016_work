﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27_07_2016_test2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi how many numbers would you like to count to?");
            var text = Console.ReadLine();
            int counter;
            bool resolve = int.TryParse( text ,out counter );
            if (resolve==true)
            { 
            var i = 0;
                for (i = 0; i < counter; i++)
                {
                    var repeat = true;
                    var a = i + 1;
                    Console.WriteLine($"OK the number is {a}");
                    do
                    {
                        var b = a + 1;
                        Console.WriteLine("What number comes next?");
                        var c = int.Parse(Console.ReadLine());
                        if (b == c)
                        {
                            Console.WriteLine("Good stuff next question");
                            repeat = false;
                        }
                        else
                        {
                            Console.WriteLine("Nope try again");
                            repeat = true;
                        }

                    } while (repeat == true);
                } 
            }
            else
            {
                Console.WriteLine("It needs to be a number");
            }
        }
    }
}
